-- Code belongs to WiSe14DT-S1T3

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;

entity tb4mealymoore is                                                   -- Device Under Test
end entity tb4mealymoore;

architecture beh of tb4mealymoore is 

    signal di_s   : std_logic_vector(1 downto 0);
    signal clk_s  : std_logic;
    signal nres_s : std_logic;

    component dut
        port (
            -- debuging only
            dip1    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
            dip2    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
            dip3    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
            dip4    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
            dip5    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
            dip6    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
            -- 
            do      :   out std_logic_vector (1 downto 0);          -- Data out
            mrk     :   out std_logic;                              -- MaRK of 1st data following pattern
            --
            di      :   in std_logic_vector( 1 downto 0 );          -- Data In
            --
            clk     :   in std_logic;                               -- CLocK
            nres    :   in std_logic                               -- Not RESet ; low active reset
        );--]port
    end component;
    --for all : dut use entity work.mealymoore( mealy );

    component sg4mealymoore 
        port (
            do      :   out std_logic_vector( 1 downto 0 );          -- Data In
            --
            clk     :   out std_logic;                               -- CLocK
            nres    :   out std_logic                               -- Not RESet ; low active reset
        );
    end component sg4mealymoore;


begin

    dut_i : dut
        port map (
            di   => di_s,
            clk  => clk_s,
            nres => nres_s
        )--]port
    ;--}mealymoore_i

    sg4mealymoore_i : sg4mealymoore
        port map (
            do   => di_s,
            clk  => clk_s,
            nres => nres_s
        )--]port
    ;--}mealymoore_i

end architecture beh;