-- Code belongs to WiSe14DT-S1T3

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;

architecture moore of mealymoore is
    signal mrk_s : std_logic;                             -- MaRK of 1st data following pattern
    --
    type state_type is (s0, s1, s2, s3, s4);
    signal state_cs : state_type := s0;
    signal state_ns : state_type := s0;
begin

    reg :
    process (clk) is
    begin
        if (clk = '1' and clk'event) then
            -- reset
            if (nres = '0') then
                state_cs <= s0;
            else         
                state_cs <= state_ns;
            end if;
        end if;
    end process reg;

    ASN:
    process(state_cs)
    variable mrk_v : std_logic;
    begin
        if (state_cs = s4) then
            mrk_v := '1';
        else
            mrk_v := '0';
        end if;
        mrk_s <= mrk_v;
    end process ASN;

    UESN:
    process (state_cs, di)
        variable state_v : state_type;
    begin
        case state_cs is
            when s0=>
                if (di = "11") then
                    state_v := s1;
                else -- "00", "01", "10"
                    state_v := s0;
                end if;
            when s1=>
                if (di = "11") then
                    state_v := s2;
                else -- "00", "01", "10"
                    state_v := s0;
                end if;
            when s2=>
                if (di = "00")then
                    state_v := s3;
                elsif (di = "11") then
                    state_v := s2;
                else-- "01", "10"
                    state_v := s0;
                end if;
            when s3=>
                if (di = "00")then
                    state_v :=s4;
                elsif (di = "11") then
                    state_v := s1;
                else-- "01", "10"
                    state_v := s0;
                end if;
            when s4=>
                if (di = "11")then
                    state_v :=s1;
                else-- "00", "01", "10"
                    state_v := s0;
                end if;
                    
        end case;
        state_ns <= state_v;
    end process UESN;

    do <= di;
    mrk <= mrk_s;
end architecture moore;
