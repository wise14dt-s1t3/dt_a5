-- Code belongs to WiSe14DT-S1T3

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;

entity dut is                                                   -- Device Under Test
    port (
        -- debuging only
        dip1    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
        dip2    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 2 clock cycle  (for debugging purpose only)
        dip3    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 3 clock cycle  (for debugging purpose only)
        dip4    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 4 clock cycle  (for debugging purpose only)
        dip5    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 5 clock cycle  (for debugging purpose only)
        dip6    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 6 clock cycle  (for debugging purpose only)
        -- 
        do      :   out std_logic_vector (1 downto 0);          -- Data out
        mrk     :   out std_logic;                              -- MaRK of 1st data following pattern
        --
        di      :   in std_logic_vector( 1 downto 0 );          -- Data In
        --
        clk     :   in std_logic;                               -- CLocK
        nres    :   in std_logic                                -- Not RESet ; low active reset
    );--]port
end entity dut;


architecture dut of dut is 

    component mealymoore
        port (
            do      :   out std_logic_vector (1 downto 0);          -- Data out
            mrk     :   out std_logic;                              -- MaRK of 1st data following pattern
            --
            di      :   in std_logic_vector( 1 downto 0 );          -- Data In
            --
            clk     :   in std_logic;                               -- CLocK
            nres    :   in std_logic                                -- Not RESet ; low active reset
        );--]port
    end component mealymoore;
    for all : mealymoore use entity work.mealymoore( mealy );


    component dbgShfReg                                                -- Device Under Test
        port (
            -- debuging only
            dip1    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
            dip2    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 2 clock cycle  (for debugging purpose only)
            dip3    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 3 clock cycle  (for debugging purpose only)
            dip4    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 4 clock cycle  (for debugging purpose only)
            dip5    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 5 clock cycle  (for debugging purpose only)
            dip6    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 6 clock cycle  (for debugging purpose only)
            --
            di      :   in std_logic_vector( 1 downto 0 );          -- Data In
            --
            clk     :   in std_logic;                               -- CLocK
            nres    :   in std_logic                                -- Not RESet ; low active reset
        );--]port
    end component dbgShfReg;
	
	signal di_cs : std_logic_vector(1 downto 0) := "00";            -- Data In

begin

	mealymoore_i : mealymoore
        port map (
            do   => do,
            mrk  => mrk,
            di   => di_cs,
            clk  => clk,
            nres => nres
        );

    dbgShfReg_i : dbgShfReg
        port map (
            dip1 => dip1,
            dip2 => dip2,
            dip3 => dip3,
            dip4 => dip4,
            dip5 => dip5,
            dip6 => dip6,
            di   => di_cs,
            clk  => clk,
            nres => nres
        );

	reg :
    process (clk) is
    begin
        if (clk = '1' and clk'event) then
            -- reset
            if (nres = '0') then
                di_cs <= "00";
            else         
                di_cs <= di;
            end if;
        end if;
    end process reg;

end architecture dut;