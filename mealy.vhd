-- Code belongs to WiSe14DT-S1T3

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;

architecture mealy of mealymoore is
    signal mrk_ns : std_logic;                             -- MaRK of 1st data following pattern
	signal mrk_cs: std_logic :='0';
    --
    type state_type is (s0, s1, s2, s3);
    signal state_cs : state_type := s0;
    signal state_ns : state_type := s0;
begin

    reg :
    process (clk) is
    begin
        if (clk = '1' and clk'event) then
            -- reset
            if (nres = '0') then
                state_cs <= s0;
				mrk_cs <= '0'; 
            else         
				
                state_cs <= state_ns;
				mrk_cs <= mrk_ns;
            end if;
        end if;
    end process reg;

    SN:
    process (state_cs, di)
        variable mrk_v : std_logic;
        variable state_v : state_type;
    begin
         case state_cs is
            when s0=>
                if (di = "11") then
                    state_v := s1;
                    mrk_v := '0';
                else -- "00", "01", "10"
                    state_v := s0;
                    mrk_v := '0';
                end if;
            when s1=>
                if (di = "11") then
                    state_v := s2;
                    mrk_v := '0';
                else -- "00", "01", "10"
                    state_v := s0;
                    mrk_v := '0';
                end if;
            when s2=>
                if (di = "00")then
                    state_v := s3;
                    mrk_v := '0';
                elsif (di = "11") then
                    state_v := s2;
                    mrk_v := '0';
                else-- "01", "10"
                    state_v := s0;
                    mrk_v := '0';
                end if;
            when s3=>
                if (di = "00")then
                    state_v :=s0;
                    mrk_v := '1';
                elsif (di = "11") then
                    state_v := s1;
                    mrk_v := '0';
                else-- "01", "10"
                    state_v := s0;
                    mrk_v := '0';
                end if;
            end case;
        state_ns <= state_v;
        mrk_ns <= mrk_v;
    end process SN;

    do <= di;
    mrk <= mrk_cs;
end architecture mealy;
