-- Code belongs to WiSe14DT-S1T3

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;

entity dbgshfReg is                                          -- Device Under Test
    port (
        -- debugging only
        dip1    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 1 clock cycle  (for debugging purpose only)
        dip2    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 2 clock cycle  (for debugging purpose only)
        dip3    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 3 clock cycle  (for debugging purpose only)
        dip4    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 4 clock cycle  (for debugging purpose only)
        dip5    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 5 clock cycle  (for debugging purpose only)
        dip6    :   out std_logic_vector (1 downto 0);          -- "Data In" Plus 6 clock cycle  (for debugging purpose only)
        --
        di      :   in std_logic_vector ( 1 downto 0 );         -- Data In
        --
        clk     :   in std_logic;                               -- CLocK
        nres    :   in std_logic                                -- Not RESet ; low active reset
        );--]port
end entity dbgshfReg;

architecture beh of dbgshfReg is
    signal shfReg_cs : std_logic_vector (9 downto 0) := ( others => '0');          -- 6dip ye 2 bit also 2x6bit = 12 bit
begin


    reg :
    process (clk) is
    begin
        if (clk = '1' and clk'event) then
            -- reset
            if (nres = '0') then
                shfReg_cs <= (others => '0');
            else         
                shfReg_cs( 9 downto 2 ) <= shfReg_cs( 7 downto 0 );
                shfReg_cs( 1 downto 0 ) <= di;
            end if;
        end if;
    end process reg;

	dip1 <= di;
    dip2 <= shfReg_cs(1 downto 0);
    dip3 <= shfReg_cs(3 downto 2);
    dip4 <= shfReg_cs(5 downto 4);
    dip5 <= shfReg_cs(7 downto 6);
    dip6 <= shfReg_cs(9 downto 8);

end architecture beh;