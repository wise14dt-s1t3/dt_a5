-- Code belongs to WiSe14DT-S1T3

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;

entity mealymoore is                                              
    port (
        do      :   out std_logic_vector ( 1 downto 0 );          -- Data out
        mrk     :   out std_logic;                              -- MaRK of 1st data following pattern
        --
        di      :   in std_logic_vector ( 1 downto 0 );          -- Data In
        --
        clk     :   in std_logic;                               -- CLocK
        nres    :   in std_logic                                -- Not RESet ; low active reset
    );--]port
end entity mealymoore;