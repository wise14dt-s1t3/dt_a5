-- Code belongs to TI3 DT WS 11/12
-- Demo für Z1
-- History:
--  111212: 1st version for TI3 DT WS11/12  by Michael Schaefers 



library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.math_real.all;                            -- für random / uniform()
    
    
    
-- sg ::= Stimuli Generator    
entity sg4mealymoore is                                           -- Stimuli Generator
    port (
        do : out std_logic_vector( 1 downto 0 );   -- TxData send to DUT
        clk    : out std_logic;                        -- CLocK
        nres   : out std_logic                         -- Not RESet ; low active reset
    );--]port
end entity sg4mealymoore;



architecture beh of sg4mealymoore is
    
    constant quarterClockCycle   : time := 25 ns;
    constant halfClockCycle      : time := 2*quarterClockCycle;
    constant fullClockCycle      : time := 2*halfClockCycle;
    
    signal   simulationRunning_s : boolean := true;
    
begin
    
    sg:                                                -- Stimuli Generator
    process is
        variable seed1_v : positive := 1;              -- 1 <= seed1 <= 2147483562  -> see IEEE.MATH_DEAL docu
        variable seed2_v : positive := 1;              -- 1 <= seed2 <= 2147483398  -> see IEEE.MATH_DEAL docu
        variable rRand_v : real;                       -- 
        variable iRand_v : integer;                    -- 
    begin
        simulationRunning_s <= true;
        
        wait for halfClockCycle;                       -- setup time
        do <= (others=>'0');
        wait for halfClockCycle;                       -- synchronous reset gone (see "resGen")
        wait for fullClockCycle;
        wait for quarterClockCycle;                    -- "align" after rising clock edge
        
        do <= "00";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;      -- <<== HERE: 3->3->0->0
        do <= "11";  wait for fullClockCycle;      -- <<== MaRK
        do <= "11";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;      -- <<== HERE: 3->3->0->0
        do <= "11";  wait for fullClockCycle;      -- <<== MaRK
        do <= "11";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;      -- <<== HERE: 3->3->0->0
        do <= "11";  wait for fullClockCycle;      -- <<== MaRK
        do <= "11";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "01";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "00";  wait for fullClockCycle;
        do <= "10";  wait for fullClockCycle;
        do <= "11";  wait for fullClockCycle;
        do <= "01";  wait for fullClockCycle;
        do <= "10";  wait for fullClockCycle;
        
--        for i in 0 to 2000 loop
--            uniform( seed1_v, seed2_v, rRand_v );      -- 0 < rRand < 1
--            if    rRand_v < 0.25 then do <= "00";
--            elsif rRand_v < 0.5  then do <= "01";
--            elsif rRand_v < 0.75 then do <= "10";
--            else                      txdata <= "11";
--           end if;
--            wait for fullClockCycle;
--        end loop;
        
        -- sg soll stoppen, sobald alle Testmuster abgearbeitet
        wait for 3*fullClockCycle;                     -- noch 10 Takte warten
        simulationRunning_s <= false;                  -- Stoppen
        wait;
    end process sg;
    
    
    
    clkGen:                                            -- CLocK GENerator
    process is
    begin
        clk <= '0';
        wait for fullClockCycle;
        while simulationRunning_s loop
            clk <= '1';
            wait for halfClockCycle;
            clk <= '0';
            wait for halfClockCycle;
        end loop;
        wait;
    end process clkGen;
    
    
    
    resGen:                                            -- RESet GENerator
    process is
    begin
        nres <= '0';
        wait for 5*quarterClockCycle;
        nres <= '1';
        wait;
    end process resGen;
    
end architecture beh;